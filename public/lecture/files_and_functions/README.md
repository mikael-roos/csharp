---
author: mos
revision: 
    2023-01-03: "(A, mos) first version"
---
C# - Files and Functions
====================

Learn how to separate your code into files and functions using C#.

<!--
The following are for example included.

* if
* switch
* while
* do
* for
-->

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/csharp/lecture/files_and_functions/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The book "Microsoft Visual C# Step by Step, 10th Edition".

    * CHAPTER 3 - Writing methods and applying scope
