---
author: mos
revision: 
    2023-01-03: "(A, mos) first version"
---
C# - Conditions and Iterations
====================

Learn how to do conditional statements and iterations using C#.

The following are for example included.

* if
* switch
* while
* do
* for

You can browse [the HTML slides used in the presentation](https://mikael-roos.gitlab.io/csharp/lecture/conditions_loops/slide.html) (press f/esc to enter/exit full screen mode and navigate using the arrows).

<!--
Recorded presentation, 44 minutes long (Swedish).

[![2022-11-15 swe](https://img.youtube.com/vi/zovl9Lvheus/0.jpg)](https://www.youtube.com/watch?v=zovl9Lvheus)
-->



Resources
------------------------

Resources that are used, or are relevant, for the areas that the lecture covers.

1. The book "Microsoft Visual C# Step by Step, 10th Edition".

    * CHAPTER 4 - Using decision statements
    * CHAPTER 5 - Using compound assignment and iteration statements


