Noteringar från fredagsträffen vecka 6
============================

[[_TOC_]]



Del 2 av kursen
----------------------------

Titta på uppgiften och dess olika delar, saknar vi något?

* Överlagring av metod



Metoder, parametrar, returntyp, överlagring
----------------------------

Bygg ett program för en miniräknare och prova skapa ett antal metoder.



### add()

* Skicka "okänd typ" som argument till metod.
* Inkommande argument, return typ
* Metod signatur
* Överlagring
* Typen `var` som lagring för godtycklig typ.

Referenser.

* [Metoder i manualen](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/methods)
* [Typen `object`](https://learn.microsoft.com/en-us/dotnet/api/system.object)
* [Typen `var` (del av manualen för typer)](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/types/)



### div()

* En div() som fungerar med int och en som fungerar med double
* vad händer vid div / 0?
* Fånga exception
* Kasta exception

> "Floating-point arithmetic overflow or division by zero never throws an exception, because floating-point types are based on IEEE 754 and so have provisions for representing infinity and NaN (Not a Number)."

Referenser.

* [Division](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/arithmetic-operators#division-operator-)
* [Double NaN](https://learn.microsoft.com/en-us/dotnet/api/system.double.nan)
* [Exception](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/exceptions/exception-handling)
    * Rethrow
    * finally()
    * [Best practices Exception](https://learn.microsoft.com/en-us/dotnet/standard/exceptions/best-practices-for-exceptions)


### Exempelprogram

Exempelprogram finns i [`src/calculator`](../../../src/calculator/).



<!--
Övrigt
----------------------------
-->