Noteringar från fredagsträffen vecka 2
============================

### Labbmiljö

Har för mig du rekommenderade Visual studio code framför vanliga Visual studio, boken och exempeluppgiften verkar vara baserat på Visual studio, vad är skillnaden?

* https://visualstudio.microsoft.com/downloads/



### Kom igång med uppgiften

* Komma igång första uppgiften.
Du får gärna prata om inlämningsuppgiften.
Exempeluppgiften i inlämningsuppgifter dvs
Hej. Jag fick tillgång till kursen för någon dag sedan.  Är det bara att ladda ner vscode och börja med uppgift 1?

[Exempel på kodstruktur för inlämningen](https://gitlab.com/mikael-roos/csharp/-/blob/main/src/HomeworkModule1/Program.cs).



### Om parsning & typkonvertering

Jag undrar lite över Parse i allmänhet, så man förstår exakt vad man gör. Det blir lite att det funkar för att man följer instruktion men inte riktigt förstår varför

* [int.parse()](https://learn.microsoft.com/en-us/dotnet/api/system.int32.parse?view=net-7.0)

Går att konvertera typ med int.Parse(input), men vet att det också går att använda Convert.ToInt32(input). Är det någon av metoderna som föredras? Någon fördel, nackdel med dem?

* [convert.toint32()](https://learn.microsoft.com/en-us/dotnet/api/system.convert.toint32?view=net-7.0)

Exempelprogram finns i `src\types-converting`.



### Datatyper

Jag undrar kring typning - kan man kombinera typningar? Ex. int och duble, eller int och string osv 🙂

* [Casting and type conversions (C# Programming Guide)](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/types/casting-and-type-conversions). Förklaring till hur typkonvertering sker, implicit och explicit.
* Exempelprogram för implicit och explicit typkonvertering, [src\types-implicit-conversion](../../src/types-implicit-conversion/).

Hej! Undrar hur man får upp en lista med olika typer av data types? Jag läste i boken Kapitel 2 under "displaying private data type values" och förstod inte riktigt hur de fick upp listan.

* [Lista med inbyggda typer i C#](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types)



### Felsökning

Har lyckats få ett felmeddelande på mina .ReadLine. Har felsökt med en kunnig kompis.



### Övrigt 

* Visa exempelprogram i exempelrepot, OldStyleApp, HomeworkModule1, ChatBotApp.


## Kodstruktur

Hur mycket ska vi fokusera på namespace, class, main osv

Tjuvkika på ett exempelprogram, [`src\ChatBotFunctionsApp`](../../src/ChatBotFunctionsApp/).
