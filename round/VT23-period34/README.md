<!--
![What now?](exercise/hello_world/img/hello_world.png)
-->

Weekly log - 2023 period 3, 4
==========================

This is an overview of what happens week by week. The content will be updated all during the course, as each week goes by.

[[_TOC_]]


<!--
TODO

* Spara och läsa innehåll från filer, tex notes. Lägg till note, ta bort note.

* Något mer exempel på arv och komposition.
    * Arv och hur man anropar base constructorn, text shape.

* Mer användning av Interface, exempel.
* Abstrakta klasser och deras användning. Exempel.

* Mer om array och listor, exempel
* Stack, Queue och Hashtable, exempel

* UI programmering
    * MV
    * MVVM
    * Behöver kunskap om async / await
    * Enklare guider eller lita på Microsofts varianter?
    * Visa olika UI kontroller och hur man kan jobba med dem, inklusive navigering.

* Fundera på vad projektet skall innehålla och hur de olika delarna leder fram till det.
* Projekt kan lösas som terminalapp eller UI app, kanske UI appar med olika svårighetsgrad.


IMPROVEMENTS

* Förbättra instruktionen till hur man installerar labbmijlön.
* Det finns en hel del grundläggande saker man kan ta upp i inledningen av kursen, men de behöver man även träna på i labbar.
    * Struktur av filer, klasser, metoder, namespace
    * Formattering av utskrift
    * Alla olika datatyper
    * Mer om inbyggda funktioner
* Varje vecka borde ha en eller flera mindra övningsuppgifter där det finns facit, och eller med labbar där man jobbar mot ett facit och själv kan kolla om det blev rätt.

-->


Prepare yourself
--------------------------

These are things you can do to prepare yourself and get going with the course.

1. [Install the lab environment](../../exercise/lab-environment/README.md).



Week 01 (w03): Get going with C#
--------------------------

Module 1 starts.

We start up the course, install the lab environment and writes our first program in C#.

Teacher activities:

* Introduction to the course setup
* [Lecture on basics with C#](../../public/lecture/basics/README.md).
* Walkthrough of the exercise [A Chatbot to learn C# input and output](../../exercise/chatbot-input-and-output/README.md).
* (Debugger)



Week 02 (w04): 
--------------------------

* [Notes from week 2](./notes/v02.md)

<!--
* visa saker "runt omkring", det borde finnas mycket att lyfta fram när det går lugnt i inledningen
* formattering av utskrift
* typer
* typkonvertering
-->



Week 03 (w05): Structure code into files and functions
--------------------------



**Teacher activities:**

* [Notes from week 3](./notes/v03.md)

<!-- Did not do these

* [Lecture on files and functions](../../public/lecture/files_and_functions/README.md).
* Walkthrough of the exercise [Structure the Chatbot into functions and files](../../exercise/chatbot-structure-files-and-functions/README.md).
* (Debugger)

-->



Week 04 (w06): If conditions and while loops
--------------------------

**Teacher activities**

* [Notes from week 4](./notes/v04.md)

<!--
Idag kommer jag ta upp if-sats och while-loop som är en del av inlämningsuppgift 2. Jag kommer använda några enkla konstruktioner och försöka sätta ihop dem till ett litet gissningsspel där datorn tänker på en siffra och jag skall försöka gissa rätt. Eventuellt kikar vi även lite på hur man kan organisera ett sådant spel i en klass med metoder. 
-->

**Work**

* Start working on A2.
* Submit A1.



Week 05 (w07): Conditions and iterations
--------------------------

(Module 2 starts.)

**Teacher activities**

* [Notes from week 5](./notes/v05.md)

<!--
* [Lecture on conditions and iterations](../../public/lecture/conditions_loops/README.md).
* Walkthrough of the exercise [Let ChatBot work with conditions and iterations](../../exercise/chatbot-conditions-and-iterations/README.md).
-->



Week 06 (w08): Methods, exceptions and error handling
--------------------------

**Teacher activities**

* [Notes from week 6](./notes/v06.md)

<!--
* [Lecture on exceptions and error handling](../../public/lecture/exception_and_error_handling/README.md).
-->



Week 07 (w09): Exceptions and error handling
--------------------------

Teacher activities:

* [Notes from week 7](./notes/v07.md)

<!--
* Walkthrough of the exercise [Let ChatBot work with exceptions and error handling](../../exercise/chatbot-exception-error/README.md).

* Perhaps code a small game like guess my number or some card game like 21 (without classes)?
    * Flow/Sequence diagram
-->



Week 08 (w10): 
--------------------------

**Teacher activities:**

* [Notes from week 8](./notes/v08.md)

**Work:**

Submit module 2 assignment.



Week 09 (w11): Classes and Objects
--------------------------

(Module 3 starts.)

**Teacher activities:**

* [Notes from week 9](./notes/v09.md)

**Work:**

Start working with module 3 assignment.

<!--
* [Lecture on exceptions and error handling](../../public/lecture/exception_and_error_handling/README.md).
* Walkthrough of the exercise [Let ChatBot work with exceptions and error handling](../../exercise/chatbot-exception-error/README.md).
-->

* Classes and Objects
* Pass by reference
* UML start
    * Class diagram



Week 10 (w12): 
--------------------------

Week 11 (w13): Classes and Objects...
--------------------------

Teacher activities:

* More Classes and Objects
* Pass by reference
* UML more and just practice?
* Build small card application



Week 12 (w14): 
--------------------------

Submit module 3 assignment.



Week 13 (w15): Inheritance and Interface
--------------------------

(Module 4 starts.)

Teacher activities:

* Datastructures
* Inheritance
* (Abstract classes and methods)
* (Interface)



Week 14 (w16): 
--------------------------

Week 15 (w17): Graphical application
--------------------------

Teacher activities:

* Graphical application with UWP


Week 16 (w18): 
--------------------------

Submit module 4 assignment.



Week 17 (w19): Project
--------------------------

(Project starts.)

UML exercise

<!--
* Should happen earlier and all through the course
* Use pseudo code earlier, and sequence diagrams 
* Use UML variant of sequence diagrams?
-->



Week 18 (w20): 
--------------------------

Week 19 (w21): 
--------------------------

Week 20 (w22): 
--------------------------

Submit project assignment.

