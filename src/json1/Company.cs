using System.Text.Json;

class Company
{
    List<Employee> Employees { get; set; } = new List<Employee>();

    public void ReadFromFile(string fileName)
    {
        string jsonString = File.ReadAllText(fileName);

        Employees = JsonSerializer.Deserialize<List<Employee>>(jsonString)
            ?? new List<Employee>();
    }

    public void EmployeesToString()
    {
        Console.WriteLine("### Company employees (as strings)");
        Employees.ForEach(Console.WriteLine);
    }

    public Employee FindEmployeeById(int id)
    {
        Employee ?emp = Employees.Find(employee => employee.Id == id);

        return emp ?? new Employee();
    }
}