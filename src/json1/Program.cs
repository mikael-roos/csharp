﻿using System.Text.Json;

Console.WriteLine("## Person");
Person p1 = new Person("Mikael", "Roos", 42, Sex.Male);
Console.WriteLine(p1);

Person p2 = new Person("Mumin", "Troll", 13, Sex.Unknown);
Console.WriteLine(p2);



// Use employee instead
Console.WriteLine("\n## Employee");
Employee emp1 = new Employee("Mikael", "Roos", 42, Sex.Male);
emp1.Id = 42;
emp1.Salary = 30000;
emp1.Position = "Manager";
Console.WriteLine(emp1);
emp1.WriteToFile();

Employee emp2 = new Employee("Mumin", "Troll", 13, Sex.Unknown);
emp2.Id = 13;
emp2.Salary = 13000;
emp2.Position = "Inspiration";
Console.WriteLine(emp2);
emp2.WriteToFile();

Employee emp3 = Employee.ReadFromFile(99);

Console.WriteLine("\n## ToJson");
Console.WriteLine(emp1);
Console.WriteLine(emp1.ToJson());
Console.WriteLine(emp2);
Console.WriteLine(emp2.ToJson());
Console.WriteLine(emp3);
Console.WriteLine(emp3.ToJson());



// Company
Console.WriteLine("\n########################");
Console.WriteLine("### Company (List)");
List<Employee> employees = new List<Employee>();

employees.Add(emp1);
employees.Add(emp2);
employees.Add(emp3);

//employees.ForEach(Console.WriteLine);
//employees.ForEach(emp => Console.WriteLine(emp.ToJson()));

var options = new JsonSerializerOptions { WriteIndented = true };
string jsonString = JsonSerializer.Serialize(employees, options);
Console.WriteLine(jsonString);

string fileName = $"data/employee.json";
File.WriteAllText(fileName, jsonString);



// Read all employees
string fileName1 = $"data/employee_backup.json";
string jsonString1 = File.ReadAllText(fileName1);

List<Employee> emps = JsonSerializer.Deserialize<List<Employee>>(jsonString1)
    ?? new List<Employee>();
emps.ForEach(Console.WriteLine);


// Use Company
Company comp = new Company();
comp.ReadFromFile("data/employee_backup.json");
comp.EmployeesToString();

Console.WriteLine("\n# Company, find employee");
Employee emp = comp.FindEmployeeById(99);
Console.WriteLine(emp.ToJson());
