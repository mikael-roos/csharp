enum Sex
{
    Male,
    Female,
    Binary,
    Unknown
}

class Person
{
    private string _firstName;
    private string _lastName;
    private int _age = 0;

    public const int MinAge = 0;
    public const int MaxAge = 99;

    private Sex _sex = Sex.Unknown;

    public string FirstName
    {
        get => _firstName;
        set => _firstName = value;
    }

    public string LastName
    {
        get => _lastName;
        set => _lastName = value;
    }

    public int Age
    {
        get => _age == 0 ? -1 : _age;
        set => _age = value > 0 ? value : -1; 
    }

    public Sex Sex
    {
        get => _sex;
        set => _sex = value;
    }

    public Person()
    {
        _firstName = "Johan/Jane";
        _lastName = "Doe";
    }

    public Person(string firstName, string lastName, int age, Sex sex)
    {
        _firstName = firstName;
        _lastName = lastName;
        _age = age;
        _sex = sex;
    }

    public override string ToString()
    {
        return _firstName + " " + _lastName + $" ({_age})" + $" {_sex}";
    }
}