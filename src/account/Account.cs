class Account
{
    private double balance = 0;

    public static int _numAccounts = 0;


    public Account() 
    {
        _numAccounts++;
    } 

    public void PrintBalance()
    {
        DateTime now = DateTime.Now;
        Console.WriteLine("{0}: Your balance is: {1:0.00}", now, balance);
    }

    public void Deposit(double amount)
    {
        balance += amount;
        PrintBalance();
    }

    public void GetBalance(out double balance)
    {
        balance = this.balance;
    }
}