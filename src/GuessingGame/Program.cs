﻿Console.WriteLine("Hello, welcome to this guessing game");

// Get a random number between 1 and 100
Random rand = new Random();
int number = rand.Next(1, 100);

int result = GuessGame.ReadNumberFromUser();
Console.WriteLine($"You did guess: {result}");
Console.WriteLine($"******");

// Get the guess from the user
string? input;
int guess;
Console.WriteLine("I am thinking of a number between 1 and 100.");

Console.Write("Make a guess? ");
input = Console.ReadLine();
bool res = int.TryParse(input, out guess);

if (!res)
{
    Console.WriteLine("ERROR: Guess is not a integer!");
}


// while (!int.TryParse(input, out guess)) {
//     Console.WriteLine("ERROR: You need to enter an integer between 1 and 100!");

//     Console.Write("Make a guess? ");
//     input = Console.ReadLine();
// }

Console.WriteLine($"You did guess: {guess}");
Console.WriteLine($"The actual number is: {number}");

