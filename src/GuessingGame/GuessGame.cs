class GuessGame
{
    public static int ReadNumberFromUser()
    {
        string? input;
        int guess;

        Console.Write("Make a guess? ");
        input = Console.ReadLine();
        bool res = int.TryParse(input, out guess);

        if (!res)
        {
            Console.WriteLine("ERROR: Guess is not a integer!");
        }
        return guess;
    }
}