﻿using System.Text.Json;

Employee emp1 = new Employee("Mikael", "Roos", 42, Sex.Male);
emp1.Salary = 30000;
emp1.Id = 42;
emp1.Position = "Manager";
Console.WriteLine(emp1);
Console.WriteLine(emp1.ToJson());
emp1.WriteToFile();

Employee emp2 = new Employee("Mumin", "Troll", 13, Sex.Unknown);
emp2.Salary = 90000;
emp2.Id = 13;
emp2.Position = "Inspiration";
emp2.WriteToFile();

Employee emp3 = Employee.ReadFromFile(99);
Console.WriteLine(emp3.ToJson());

// List (Company)
List<Employee> employees = new List<Employee>();

employees.Add(emp1);
employees.Add(emp2);
employees.Add(emp3);

employees.ForEach(Console.WriteLine);
employees.ForEach(employee => Console.WriteLine(employee.ToJson()));

string fileName = "data/employees.json"; 
var options = new JsonSerializerOptions { WriteIndented = true };
string jsonString = JsonSerializer.Serialize(employees, options);
File.WriteAllText(fileName, jsonString);

Console.WriteLine(File.ReadAllText(fileName));

// Read back
fileName = "data/employees_backup.json"; 
string jsonString1 = File.ReadAllText(fileName);

List<Employee> employees1 = JsonSerializer.Deserialize<List<Employee>>(jsonString1)
    ?? new List<Employee>();
employees1.ForEach(Console.WriteLine);


// Try Company
Console.WriteLine("### Company");
Company comp = new Company();

comp.ReadFromFile("data/employees_backup.json");
comp.EmployeesToString();
comp.EmployeesToJson();


Console.WriteLine("# Company, find employee");
Employee emp = comp.FindEmployeeById(99);
Console.WriteLine(emp.ToJson());

