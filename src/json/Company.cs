using System.Text.Json;

class Company
{
    public List<Employee> Employees = new List<Employee>();

    public void ReadFromFile(string fileName)
    {
        string jsonString1 = File.ReadAllText(fileName);

        Employees = JsonSerializer.Deserialize<List<Employee>>(jsonString1)
            ?? new List<Employee>();
    }

    public void EmployeesToString()
    {
        Console.WriteLine($"Employee list as string ({Employees.Count})");
        Employees.ForEach(Console.WriteLine);
    }

    public void EmployeesToJson()
    {
        Console.WriteLine($"Employee list as json ({Employees.Count})");
        Employees.ForEach(employee => Console.WriteLine(employee.ToJson()));
    }

    public Employee FindEmployeeById(int id)
    {
        Employee ?emp = Employees.Find(employee => employee.Id == id);

        return emp ?? new Employee();
    }
}