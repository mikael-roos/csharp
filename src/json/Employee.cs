using System.Text.Json;

class Employee : Person
{
    public int Salary { get; set; } = 0;
    public int Id { get; set; } = 0;
    public string Position { get; set; } = "Unknown";

    public Employee() : base()
    { }

    public Employee(
        string firstName,
        string lastName,
        int age, 
        Sex sex
    ) : base(firstName, lastName, age, sex)
    { }

    public override string ToString()
    {
        return base.ToString() + $", ({Id}) works as '{Position}' for ${Salary}.";
    }

    public string ToJson()
    {
        var options = new JsonSerializerOptions { WriteIndented = true };
        string jsonString = JsonSerializer.Serialize(this, options);

        return jsonString;
    }

    public void WriteToFile()
    {
        string fileName = $"data/employee_{Id}.json"; 
        var options = new JsonSerializerOptions { WriteIndented = true };
        string jsonString = JsonSerializer.Serialize(this, options);
        File.WriteAllText(fileName, jsonString);

        Console.WriteLine(File.ReadAllText(fileName));
    }

    public static Employee ReadFromFile(int id)
    {
        string fileName = $"data/employee_{id}.json"; 
        string jsonString = File.ReadAllText(fileName);
        Employee ?emp = JsonSerializer.Deserialize<Employee>(jsonString);
        //Console.WriteLine(emp.ToJson());
        return emp ?? new Employee();
    }
}