﻿using DiceGame;

Console.WriteLine("### Play Dice ###");

Dice die = new Dice();
die.Roll();
Console.WriteLine(die);

DiceGraphic die1 = new DiceGraphic();
die1.Roll();
Console.WriteLine(die1);

Console.WriteLine("### Dicehand ###");
DiceHand hand = new DiceHand();
hand.Add(die);
hand.Add(die1);
hand.Add(new DiceGraphic());

IDiceInterface die2 = new Dice();
hand.Add(die2);

IDiceInterface die3 = new DiceGraphic();
hand.Add(die3);

hand.Roll();
Console.WriteLine(hand);
Console.WriteLine(hand.Values());

//Console.WriteLine(die1.Representation());