namespace DiceGame;

class DiceGraphic : Dice
{
    char[] _graphic = {'⚀', '⚁', '⚂', '⚃', '⚄', '⚅'};

    public override string ToString()
    {
        return _graphic[_value - 1].ToString();
    }

    // new public string Representation()
    public override string Representation()
    {
        //Console.WriteLine("x");
        return _graphic[_value - 1].ToString();
    }

}