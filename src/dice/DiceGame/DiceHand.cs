namespace DiceGame;

class DiceHand
{
    private List<IDiceInterface> _dice = new List<IDiceInterface>();

    public void Add (IDiceInterface die)
    {
        _dice.Add(die);
    }

    public void Roll ()
    {
        _dice.ForEach(die => die.Roll());
    }

    public override string ToString()
    {
        string res = "";
        foreach(var die in _dice) 
        {
            //Console.WriteLine("++" + die);
            //Console.WriteLine("--" + die.Representation());
            res += die.Representation() + " ";
        }
        return res;
    }

    public string Values()
    {
        string res = "";
        foreach(var die in _dice) 
        {
            res += die.Value() + ", ";
        }
        return res;
    }
}