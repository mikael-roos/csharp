namespace DiceGame;

class Dice : IDiceInterface
{
    protected int _value;

    public int Roll () 
    {
        var rand = new Random();
        _value = rand.Next(1, 6);
        return _value;
    }

    public override string ToString()
    {
        return $"[{_value}]";
    }

    public virtual string Representation()
    {
        return $"[{_value}]";
    }

    public int Value()
    {
        return _value;
    }
}
