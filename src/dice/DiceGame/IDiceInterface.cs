namespace DiceGame;

interface IDiceInterface
{
    int Roll ();
    
    string Representation();
 
    int Value();
}