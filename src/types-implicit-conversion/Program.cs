﻿// Implicit and explicit type converting
// https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/types/casting-and-type-conversions
//
Console.WriteLine("We start with a set of types and values.");

int a = 3;
double b = 3.1415;
int resInt;
double resDouble;

Console.WriteLine("int a = " + a);
Console.WriteLine("double b = " + b);

Console.WriteLine("Here are the results from some implicit type conversion.");
Console.WriteLine("a + b = " + (a + b));

Console.WriteLine("Here are the results from storing results in int variable, including a cast.");
resInt = a + (int) b;
Console.WriteLine("resInt = a + (int) b = " + resInt);

Console.WriteLine("Here are the results from storing results in double variable");
resDouble = a + b;
Console.WriteLine("resDouble = a + b = " + resDouble);
