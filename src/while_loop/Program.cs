﻿Console.WriteLine("Do some while looping!");

string name = "Mikael the great";
int number = 0;

while (number <= 7)
{
    number++;

    if (number % 2 == 0)
    {
        Console.WriteLine($"{number} {name}");
    }
}

/*
while (true) {
    number++;

    //if (number == 4 || number == 5 || number == 7) {
    //if (number % 2 == 0) {
    if (number % 2 == 1) {
        continue;
    }

    Console.WriteLine($"{number} {name}");

    if (number >= 7) {
        break;
    }
}
*/
/*
number++;
number = number + 1;
number += 1;
*/