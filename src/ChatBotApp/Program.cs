﻿string banner = @"
(  \/  )  /__\  (  _ \( \/ )(_  _)( \( )   / __)( )_( )  /__\ (_  _)  (  _ \(  _  )(_  _)
 )    (  /(__)\  )   / \  /  _)(_  )  (   ( (__  ) _ (  /(__)\  )(     ) _ < )(_)(   )(  
(_/\/\_)(__)(__)(_)\_)  \/  (____)(_)\_)   \___)(_) (_)(__)(__)(__)   (____/(_____) (__) ";
string marvin = @"
                      ,     ,
                     (\____/)
Hello, I am Marvin!   (_oo_)
                        (O)
What can I do         __||__    \/
 you for?          []/______\[] /
                   / \______/ \/
                  /    /__\
                 /\   /____\
";

Console.WriteLine(banner);
Console.WriteLine(marvin);

// Get the name from the user
Console.Write("Hello friend! What is your name: ");
string? name = Console.ReadLine();

// Say Hi using the name variable
string helloMessage = $"Hello {name}! Nice, what may I call you? ";
Console.Write(helloMessage);

// Get the name by which the user wants to be called and say hi again
string? caller = Console.ReadLine();
string callYouMessage = $"Nice {name}, you are now known as {caller}, welcome!\nNow, lets start!";
Console.WriteLine(callYouMessage);



// Get current age and age of retirement
string header = "Plan the retirement";
string separator = "**********";

Console.Write("Press Enter to continue. ");
Console.ReadLine();
Console.WriteLine(String.Format("\n{0} {1, -30} {0}", separator, header));

Console.Write("What is your current age? ");
int currentAge = Convert.ToInt32(Console.ReadLine());

Console.Write("At what age do you plan to retire? ");
int retireAge = Convert.ToInt32(Console.ReadLine());

Console.Write("How much money do you save each month? ");
int saveMonthly = Convert.ToInt32(Console.ReadLine());

int yearsToRetirement = retireAge - currentAge;
int monthsToRetirement = yearsToRetirement * 12;
int moneySavedWhenRetire = monthsToRetirement * saveMonthly;
int moneySavedYearly = saveMonthly * 12;
int ageWhenMillionaire = 1000000 / saveMonthly / 12 + currentAge;
string response = $@"
 You have {yearsToRetirement} years left to retirement and if you
 save {saveMonthly:N0} money each month (yearly {moneySavedYearly:N0} money), then 
 then you can retire at the age of {retireAge} with {moneySavedWhenRetire:N0} money
 in your savings account.
 By the way, you will become a millionaire at age {ageWhenMillionaire}.
";
Console.WriteLine(response);



// Calculate aspects of a circle
header = "Calculate the circle";
separator = "**********";

Console.Write("Press Enter to continue. ");
Console.ReadLine();
Console.WriteLine(String.Format("\n{0} {1, -30} {0}", separator, header));

Console.Write("What is the diameter of the circle? [double] ");
double diameter = Convert.ToDouble(Console.ReadLine());

double radius = diameter / 2;
double area = Math.PI * Math.Pow(radius, 2);
double circumference = Math.PI * 2 * radius;

response = $@"
 The circle diameter is {diameter:N} and the radius is {radius:N}.
 The circle circumference is {circumference:N2}.
 The circle area is {area:N2}.
 The value of PI is {Math.PI:N9}.
";
Console.WriteLine(response);



// Work with dates
header = "What is the date?";
separator = "**********";

Console.Write("Press Enter to continue. ");
Console.ReadLine();
Console.WriteLine(String.Format("\n{0} {1, -30} {0}", separator, header));

Console.Write("Enter a date as yyyy-mm-dd [default today]: ");
string date = Console.ReadLine() ?? DateTime.Today.ToString();

DateTime today = DateTime.Now;
DateTime parsedDate = DateTime.Parse(date);

response = $@"
 The date of today is {today:d} and time is currently {today:t}.
 You entered the date {date} which is read as date {parsedDate:D}.
 The year is {parsedDate.Year} and the day of the year is {parsedDate.DayOfYear}.
 The month number is {parsedDate.Month} and the date of the month is {parsedDate.Day}.
 The day of the week is {parsedDate.DayOfWeek} ({parsedDate.DayOfWeek:D}).
";
Console.WriteLine(response);
