enum SexType
{
    Male,
    Female,
    Binary,
    Unknown
}

class Person
{
    private string _firstName;
    private string _lastName;
    private int _age;
    private SexType _sex = SexType.Unknown;

    public SexType Sex
    {
        get => _sex;
        set => _sex = value;
    }

    public const int MinAge = 0;
    public const int MaxAge = 99;

    public string FirstName
    {
        get => _firstName;
        set => _firstName = value;
    }

    public int Age
    {
        //get => _age < 0 ? -1 : _age;
        get
        {
            if (_age < 0)
                return -999;
            else
                return _age;
        }
        // set => _age = value > 99 ? -1 : value;
        set
        {
            if (value > MaxAge || value < MinAge)
            {
                _age = 0;
            }
            else 
            {
                _age = value;
            }
        } 
    }

    public Person()
    {
        //Console.WriteLine("Constructor()");
        _firstName = "John/Jane";
        _lastName = "Doe";
        _age = 0;
    }

    public Person(string firstName, string lastName, int age)
    {
        //Console.WriteLine("Constructor(firstname, lastename, age)");
        _firstName = firstName;
        _lastName = lastName;
        _age = age;
    }

    public override string ToString()
    {
        //Console.WriteLine("ToString()");
        return _firstName + " " + _lastName + $" ({Age}) [{_sex}]";
    }
}