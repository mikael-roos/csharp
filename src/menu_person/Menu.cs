class Menu
{
    private static Person p1;

    public static void Run()
    {
        string input;
        string prompt = "[marvin] ";

        //Person p1 = new Person();
        p1 = new Person();

        Welcome();
        Console.WriteLine("Welcome, enter what to do, m or h for menu.");

        while (true)
        {
            Console.Write(prompt);
            input = Console.ReadLine() ?? "";

            switch (input)
            {
                case "1":
                    Console.WriteLine("1");
                    break;
                case "2":
                    Console.WriteLine("Skapa ny person");
                    p1 = new Person();
                    Console.WriteLine(p1);
                    break;
                case "3":
                    Console.WriteLine(p1);
                    break;
                case "4":
                    //ChangePersonName(p1);
                    ChangePersonName();
                    break;
                case "h":
                case "help":
                case "m":
                case "menu":
                    PrintMenu();
                    break;
                case "q":
                case "quit":
                    Console.WriteLine("Bye bye");
                    return;
                default:
                    Console.WriteLine("Not a valid choice, enter h/m for menu.");
                    break;
            }
        }
    }

    private static void PrintMenu()
    {
        string menu = @"
    ********** MENU **********
    1) Do something
    2) Skapa ny person
    3) Skriv ut person
    4) Byt förnamn på person
    m) Menu
    q) Quit
";

        Console.WriteLine(menu);
    }

    private static void Welcome()
    {
        string marvin = @"
                      ,     ,
                     (\____/)
Hello, I am Marvin!   (_oo_)
                       (O)
What can I do         __||__    \/
you for?          []/______\[]  /
                   / \______/ \/
                  /    /__\
                 /\   /____\
";

        Console.WriteLine(marvin);
    }


    //private static void ChangePersonName(Person p1)
    private static void ChangePersonName()
    {
        Console.Write("Byt namn på personen: ");
        string input = Console.ReadLine() ?? "";
        p1.FirstName = input;
    }
}