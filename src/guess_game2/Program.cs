﻿GuessingGame.WelcomeMessage();

// Dator gissar på en siffra mellan 1 till 100
int number = GuessingGame.FindNewNumber();

GuessingGame.Hint(number);

bool success;
int guess;
do
{
    // Be spelaren gissa på en siffra
    guess = GuessingGame.MakeGuess();

    // Check if correct
    success = GuessingGame.CheckGuess(number, guess);
} while (!success);

// Skriv ut spelarens gissning och svaret
Console.WriteLine($"Guess was {guess} and number was {number}");
