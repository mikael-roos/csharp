﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Typkonvertering int - sträng");

int theValue;
string theString;

Console.Write("Read a number, please: ");
theString = Console.ReadLine() ?? "";
Console.WriteLine("As string: " + theString);

//theValue = Int32.TryParse(theString);
//Console.WriteLine("As int (tryparse): " + theValue);

int number;

bool success = int.TryParse(theString, out number);
if (success) {
    Console.WriteLine($"Converted '{theString}' to {number}.");
} else {
    Console.WriteLine($"Attempted conversion of '{theString ?? "<null>"}' failed.");
}

//
try {
    theValue = Int32.Parse(theString ?? "");
    Console.WriteLine("As int: " + theValue);
}
catch (Exception e) {
    Console.WriteLine("Exception was thrown!");
    Console.WriteLine(e);
}

//
int anotherValue;

anotherValue = Convert.ToInt32(theString);
Console.WriteLine("As int (again): " + anotherValue);

