class Account
{
    private double balance = 0.0;
    private string name;

    public Account(string name)
    {
        this.name = name;
    }

    public Account(string name, double balance)
    {
        this.name = name;
        this.balance = balance;
    }

    public void PrintBalance()
    {
        DateTime now = DateTime.Now;
        Console.WriteLine(
            "{0} [{2}] Balance is currently: {1:0.00}",
            now,
            balance,
            name
        );
    }

    public void Deposit(double amount)
    {
        balance += amount;
        PrintBalance();
    }

    public void Withdrawel(double amount)
    {
        balance -= amount;
        PrintBalance();
    }

    public double GetBalance()
    {
        return balance;
    }
}