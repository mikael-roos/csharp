class Menu
{
    public static void Run()
    {
        string input;
        string prompt = "[prompt] ";

        Welcome();
        Console.WriteLine("Write what to do, or enter m/menu/h/help for your options.");

        while (true)
        {
            Console.Write(prompt);
            input = Console.ReadLine() ?? "";

            switch (input)
            {
                case "1":
                    Console.WriteLine("1");
                    break;
                case "2":
                    Console.WriteLine("2");
                    break;
                case "3":
                    Console.WriteLine("3");
                    break;
                case "4":
                    Console.WriteLine("4");
                    break;
                case "help":
                case "h":
                case "menu":
                case "m":
                    PrintMenu();
                    break;
                case "quit":
                case "q":
                    Console.WriteLine("Bye bye");
                    return;
                default:
                    Console.WriteLine("Not a valid choice, try again or m, menu to show the menu!");
                    break;
            }
        }
    }

        private static void PrintMenu()
    {
        string menu = @"
    ********** MENU **********
    1) Do something
    2) Do something
    3) Do something
    4) Do something
    m) Menu
    q) Quit
";

        Console.WriteLine(menu);
    }

    public static void Welcome()
    {
        string marvin = @"
                      ,     ,
                     (\____/)
Hello, I am Marvin!   (_oo_)
                       (O)
What can I do         __||__    \/
you for?          []/______\[]  /
                   / \______/ \/
                  /    /__\
                 /\   /____\
";

        Console.WriteLine(marvin);
    }
}