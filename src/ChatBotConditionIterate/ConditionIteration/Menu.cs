﻿namespace Mos.ConditionIteration;

class Menu {

    public static void Run(string label = "prompt") {
        string input;
        string prompt = $"[{label}] ";
        
        PrintBanner();
        Console.WriteLine("Write what to do, or enter m/menu/h/help for your options.");

        Card card = new Card();

        while (true) {
            Console.Write(prompt);
            input = Console.ReadLine() ?? "";

            switch (input) {
                case "1":
                    byte valueOfCard = card.Flip();
                    Console.WriteLine($"The card is now value {valueOfCard}.");
                    break;
                case "2":
                    Console.WriteLine($"The card is of the suite: '{card.GetSuiteName()}'.");
                    break;
                case "3":
                    Console.WriteLine($"The color of the card is '{card.GetColor()}'.");
                    break;
                case "4":
                    Console.WriteLine($"Print the card using 'ToString()':\n{card}");
                    break;
                case "5":
                    Console.WriteLine($"Draw cards until the sum reaches 21 or above.");
                    int sum = 0;
                    while (sum < 21) {
                        card.Flip();
                        sum += card.GetValue();
                        Console.WriteLine(card);
                        Console.WriteLine($"Sum is: {sum}.");
                    }
                    break;
                case "6":
                    Console.WriteLine($"Draw cards as long as its higher than the card before.");
                    byte lastValue = 0;
                    byte currentValue = 0;
                    do {
                        lastValue = currentValue;
                        card.Flip();
                        currentValue = card.GetValue();
                        Console.WriteLine(card);
                    } while (currentValue >= lastValue);
                    break;
                case "7":
                    Console.WriteLine($"Draw cards as long as its higher than the card before.");
                    Console.WriteLine($"Card 7 is free and J of Space is end of game.");
                    lastValue = 0;
                    currentValue = 0;
                    do {
                        lastValue = currentValue;
                        card.Flip();
                        currentValue = card.GetValue();
                        Console.WriteLine(card);
                        if (currentValue == 7 || currentValue == 1) {
                            currentValue = lastValue = 0;
                            continue;
                        }
                        if (card.GetFace() == "J" && card.GetSuiteName() == "spade") {
                            break;
                        }
                    } while (currentValue >= lastValue);
                    break;
                case "8":
                    Console.WriteLine($"Draw a handfull of cards.");
                    for (int i = 0; i < 5; i++) {
                        card.Flip();
                        Console.Write(card.GetCard());
                    }
                    Console.WriteLine();
                    break;
                case "9":
                    Console.WriteLine($"Draw two hands with 5 cards.");
                    for (int j = 1; j <= 2; j++) {
                        Console.WriteLine($"Dealing hand  {j}.");
                        for (int i = 0; i < 5; i++) {
                            card.Flip();
                            Console.Write(card.GetCard());
                        }
                        Console.WriteLine();
                    }
                    break;
                case "help":
                case "h":
                case "menu":
                case "m":
                    PrintMenu();
                    break;
                case "back":
                case "b":
                case "quit":
                case "q":
                    Console.WriteLine("Bye bye");
                    return;
                default:
                    Console.WriteLine("Not a valid choice, try again or m, menu to show the menu!");
                    break;
            }
        }
    }

    private static void PrintMenu() {

        string menu = @"
    ********** MENU Conditions & Iterations **********
    1) Flip a new card
    2) Get suite of card
    3) Get color of card
    4) Print using 'ToString()'
    5) Draw cards until 21
    6) Draw cards until higher
    7) Draw cards until higher (7/A restarts)
    8) Draw five cards
    9) Draw five cards in two hands
    m|h) Menu
    q|b) Quit/Back
";

        Console.WriteLine(menu);
    }

    public static void PrintBanner() {

string marvin = @"
                    ,     ,
                    (\____/)
Lets practice        (_oo_)
conditions and        (O)
iterations!          __||__    \/
                  []/______\[] /
                   / \______/ \/
Write help/menu   /    /__\
to see the menu. /\   /____\
";

        Console.WriteLine(marvin);
    }
}
