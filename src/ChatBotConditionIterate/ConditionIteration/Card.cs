﻿namespace Mos.ConditionIteration;

class Card {

    byte card;

    public byte Flip() {
        var rand = new Random();

        card = (byte) rand.Next(1, 52);
        return card;
    }

    public string GetSuiteName() {
        string suite;
        if (card >= 1 && card <= 13) {
            suite = "heart";
        } else if (card >= 14 && card <= 26) {
            suite = "spade";
        } else if (card >= 27 && card <= 39) {
            suite = "diamonds";
        } else {
            suite = "clubs";
        }

        return suite;
    }

    public string GetColor() {
        // string color;
        // if ((card >= 1 && card <= 13) || (card >= 27 && card <= 39)) {
        //     color = "red";
        // } else if ((card >= 14 && card <= 26) || (card >= 40 && card <= 52)) {
        //     color = "black";
        // } else {
        //     color = "unknown";
        // }

        // return color;
        string color;
        int suite = (card - 1) / 13;
        switch (suite) {
            case 0:
            case 2:
                color = "red";
                break;
            case 1:
            case 3:
                color = "black";
                break;
            default:
                color = "unknown";
                break;
        }

        return color;
    }

    public byte GetValue() {
        return (byte) ((card - 1) % 13 + 1);
    }

    public string GetFace() {
        byte valueOfCard = GetValue();
        string face = (string) valueOfCard.ToString();

        switch (valueOfCard) {
            case 1:
                face = "A";
                break;
            case 11:
                face = "J";
                break;
            case 12:
                face = "D";
                break;
            case 13:
                face = "K";
                break;
        } 
        return face;
    }

    public string GetSuite () {
        int suite = (card - 1) / 13;
        string res = "";
        switch (suite) {
            case 0:
                res = "♥";
                break;
            case 1:
                res = "♠";
                break;
            case 2:
                res = "♠";
                break;
            case 3:
                res = "♠";
                break;
        }

        return res;
    }

    public string GetCard () {
        string face = GetFace();
        string suite = GetSuite();

        return $"[{face}{suite}]";
    }

    public override string? ToString () {
        string face = GetFace();
        string color = GetColor();
        string suite = GetSuiteName();

        return $"({card}) {face} of {suite} ({color})";
    }
}
