﻿Console.WriteLine("### Work with persons ###");

Person p1 = new Person();

Console.WriteLine(p1);

Console.WriteLine(p1.FirstName);
p1.FirstName = "Ken";
Console.WriteLine(p1);

Console.WriteLine(p1.Age);
p1.Age = -27;
Console.WriteLine(p1);

Console.WriteLine(p1.Sex);
p1.Sex = Sex.Male;
Console.WriteLine(p1);
