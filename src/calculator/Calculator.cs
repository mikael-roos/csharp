class Calculator
{
    public static void Print(object res)
    {
        Console.WriteLine($"Result: {res} ({res.GetType()})");
        /*
        Console.WriteLine("Result: {0:0.00} ({1})",
            res, 
            res.GetType()
        );
        */
    }

    public static int Add(int a, int b)
    {
        Console.WriteLine($"# Add as int, {a} + {b}");
        return a + b;
    }

    public static double Add(double a, double b)
    {
        Console.WriteLine($"# Add as double, {a} + {b}");
        return a + b;
    }

    public static double Div(int a, int b)
    {
        Console.WriteLine($"# Div as int, {a} / {b}");
        return a / b;
    }

    public static double Div(double a, double b)
    {
        Console.WriteLine($"# Div as double, {a} / {b}");

        if (b == 0)
        {
            throw new DivideByZeroException("No no - div by zero!");
        }
        return a / b;
    }
}