﻿Calculator.Print("Calculator redo");
/* 
int resi = Calculator.Add(1, 1);
Calculator.Print(resi);

double resd = Calculator.Add(2.1, 1.1);
Calculator.Print(resd);
*/
 
/*
var res = Calculator.Add(1.5, 1.5);
Calculator.Print(res);

res = Calculator.Add(2, 1);
Calculator.Print(res);
*/

double res;
try 
{
    res = Calculator.Div(4, 1);
    Calculator.Print(res);
}
catch (System.DivideByZeroException ex)
{
    Console.WriteLine("Caught int exception");
    Console.WriteLine(ex.Message);
    //throw;
}
finally
{
    Console.WriteLine("Finally int");
}

try
{
    res = Calculator.Div(4.5, 1);
    Calculator.Print(res);
}
catch (System.DivideByZeroException ex)
{
    Console.WriteLine("Caught double exception");
    Console.WriteLine(ex.Message);
}
finally
{
    Console.WriteLine("Finally double");
}