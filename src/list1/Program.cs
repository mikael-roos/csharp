﻿Console.WriteLine("# List");

List<int> l1 = new List<int>();

l1.Add(1);
l1.Add(2);
l1.Add(3);
l1.Add(5);
l1.Add(4);

l1.RemoveAt(0);

int temp = l1[2];
l1[2] = l1[3];
l1[3] = temp;

Console.WriteLine(l1);
Console.WriteLine("Count " + l1.Count);

foreach(var item in l1) 
{
    Console.WriteLine(item);
}

// Account
Account my = new Account("my", 100);
Account yo = new Account("yo");

List<Account> accounts = new List<Account>();

Console.WriteLine(accounts);
Console.WriteLine("Count: " + accounts.Count);

accounts.Add(my);
accounts.Add(yo);
accounts.ForEach(account => Console.WriteLine(account));
Console.WriteLine("Count: " + accounts.Count);

accounts.Add(new Account("mi", 99));
accounts.ForEach(Console.WriteLine);
Console.WriteLine("Count: " + accounts.Count);

accounts.RemoveAt(accounts.Count - 2);
accounts.ForEach(Console.WriteLine);
Console.WriteLine("Count: " + accounts.Count);
