﻿Console.WriteLine("Try some if statements");

//bool winter = false;
//bool summer = false;

string season = "spring";

if (season == "winter")
{
    Console.WriteLine("It is winter outside!");
}
else if (season == "summer")
{
    Console.WriteLine("It is summer and HOT!");
}
else if (season == "spring")
{
    Console.WriteLine("It is spring and the flowers are growing!");
}
else
{
    Console.WriteLine("It is unknown what conditions are outside!?");
}



/*
if (winter) {
    Console.WriteLine("It is winter outside!");
} else if (summer) {
    Console.WriteLine("It is summer and HOT!");
} else {
    Console.WriteLine("It is unknown what conditions are outside!?");
}
*/

/*
if (winter) {
    Console.WriteLine("It is winter outside!");
} else {
    Console.WriteLine("It is NOT winter outside!");
}*/




/*
if (winter) {
    Console.WriteLine("It is winter outside!");
}

if (!winter) {
    Console.WriteLine("It is NOT winter outside!");
}*/