﻿Console.WriteLine("Do some do-while looping!");

string name = "Mikael the great";
int number = 0;

do
{
    number++;

    if (number % 2 == 1)
    {
        Console.WriteLine($"{number} {name}");
    }
} while (number <= 7);

/* number = 0;
while (number <= 7)
{
    number++;

    if (number % 2 == 1)
    {
        Console.WriteLine($"{number} {name}");
    }
}
 */