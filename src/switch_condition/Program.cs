﻿Console.WriteLine("Try some switch case statements");

string season = "january";

string result;

switch (season)
{
    case "winter":
    case "january":
        result = "It is winter outside!";
        break;
    case "summer":
        result = "It is summer and HOT!";
        break;
    case "spring":
        result = "It is spring and the flowers are growing!";
        break;
    case "autumn":
        result = "It is autumn and the leaves are red and falling from the trees!";
        break;
    default:
        result = "It is unknown what conditions are outside!?";
        break;
}

Console.WriteLine(result);

/* if (season == "winter")
{
    Console.WriteLine("It is winter outside!");
}
else if (season == "summer")
{
    Console.WriteLine("It is summer and HOT!");
}
else if (season == "spring")
{
    Console.WriteLine("It is spring and the flowers are growing!");
}
else
{
    Console.WriteLine("It is unknown what conditions are outside!?");
}
 */