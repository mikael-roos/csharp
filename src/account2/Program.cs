﻿Console.WriteLine("Hello and welcome to the Bank!");

/* Date d = new Date();

Console.WriteLine(d.Month);

d.Month = 12;
Console.WriteLine(d.Month);

try
{
    d.Month = 13;
    Console.WriteLine(d.Month);
}
catch (Exception ex)
{
    Console.WriteLine("Something bad happened");
    //Console.WriteLine(ex);
}


Environment.Exit(0);
 */

Account my = new Account("my", 100);
Account yo = new Account("yo");

my.PrintBalance();
//my.balance = 1000000;
my.Deposit(20);

yo.PrintBalance();
yo.Deposit(1000);

my.Deposit(20);
my.Withdrawel(99);

double balance = my.GetBalance();
Console.WriteLine(balance);

balance = 0;
my.GetBalance(out balance);
Console.WriteLine(balance);

my.SetBalance(9999);
my.PrintBalance();

yo.Balance = 9999;
Console.WriteLine(yo.Balance);
yo.PrintBalance();

try
{
    yo.Balance = -1;
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
    yo.PrintBalance();
} 

Console.WriteLine("Number of accounts: {0}", yo.NumAccounts);

//Console.WriteLine("Number of accounts: {0}", Account._numAccounts);

Console.WriteLine("Number of accounts: {0}", my.NumAccounts);

