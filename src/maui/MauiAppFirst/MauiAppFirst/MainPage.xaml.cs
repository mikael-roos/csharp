﻿namespace MauiAppFirst;

public partial class MainPage : ContentPage
{
	int count = 0;

	public MainPage()
	{
		InitializeComponent();
	}

	private void OnCounterClicked(object sender, EventArgs e)
	{
		count *= 2;

		if (count == 0)
		{
			count = 1;
			CounterBtn.Text = $"Double {count} time";
		}
		else
			CounterBtn.Text = $"Double {count} times";

		SemanticScreenReader.Announce(CounterBtn.Text);
	}

    private void OnEntryTextChanged(object sender, TextChangedEventArgs e)
    {	
        NoticeField.Text = $"The entry field was changed from '{e.OldTextValue}' to '{e.NewTextValue}'";
        SemanticScreenReader.Announce(NoticeField.Text);
    }

    private void OnEntryCompleted(object sender, EventArgs e)
    {
        NoticeField.Text = $"The entry field was completed with the value '{EntryField.Text}'";
        SemanticScreenReader.Announce(NoticeField.Text);
    }
}

