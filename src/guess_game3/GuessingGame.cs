static class GuessingGame
{
    public static int MaxGuesses = 3;

    public static void Run()
    {
        string? input;

        do
        {
            GuessingGame.WelcomeMessage();

            // Dator gissar på en siffra mellan 1 till 100
            int number = GuessingGame.FindNewNumber();

            GuessingGame.Hint(number);

            int noGuesses = 0;
            bool success;
            int guess = 0;
            do
            {
                // Manage number of guesses
                noGuesses++;
                if (noGuesses > MaxGuesses) {
                    Console.WriteLine("Max guesses reached, end of game");
                    break;
                }
                Console.WriteLine($"Do guess number: {noGuesses}/{MaxGuesses}");

                // Be spelaren gissa på en siffra
                guess = GuessingGame.MakeGuess();

                // Check if correct
                success = GuessingGame.CheckGuess(number, guess);
            } while (!success);

            // Skriv ut spelarens gissning och svaret
            Console.WriteLine($"Guess was {guess} and number was {number}");

            Console.Write("Play again (y/n): ");
            input = Console.ReadLine();
        } while (input == "y");

        Console.WriteLine("Thanks for playing!");
    }



    public static void WelcomeMessage()
    {
        Console.WriteLine("Guessing Game Welcome!");
    }

    public static int FindNewNumber()
    {
        // Dator gissar på en siffra
        // mellan 1 till 100
        Random rand = new Random();
        int number = rand.Next(1, 100);
        Console.WriteLine(number);

        return number;
    }

    public static int MakeGuess()
    {
        // Be spelaren gissa på en siffra
        Console.Write("Guess a number: ");
        string? input = Console.ReadLine();

        // Dubbelkolla att siffran är en int
        int guess;
        bool res = int.TryParse(input, out guess);

        if (!res)
        {
            Console.WriteLine("ERROR: It was not an integer!");
        }

        // Dubbelkolla att siffran >= 1
        // Dubbelkolla att siffran <= 100
        if (guess < 1 || guess > 100)
        {
            Console.WriteLine("ERROR: Guess must be between 1 and 100");
        }

        return guess;
    }

    public static void Hint(int number)
    {
        // Ge spelaren en hint
        // Är siffran jämn, udda, större/mindre än 50
        if (number % 2 == 0)
        {
            Console.WriteLine("HINT: Number is even");
        }
        else
        {
            Console.WriteLine("HINT: Number is odd");
        }
    }

    public static bool CheckGuess(int number, int guess)
    {
        // Jämför spelarens gissning med siffran
        // är den samma, skriv ut "VINST"
        // Skriv ut om siffran är "STÖRRE" eller "MINDRE"
        // än gissningen
        // annars skriv ut "FEL, FÖRSÖK IGEN"
        if (number == guess)
        {
            Console.WriteLine("CORRECT!");
            return true;
        }
        else if (guess > number)
        {
            Console.WriteLine("The number is lower!");
        }
        else
        {
            Console.WriteLine("The number is higher!");
        }
        return false;
    }
}