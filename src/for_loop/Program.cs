﻿Console.WriteLine("Do some for looping!");

string name = "Mikael the great";

for (int i = 0; i <= 7; i++)
{
    if (i % 2 == 1)
    {
        Console.WriteLine($"{i} {name}");
    }
}

for (int i = 0; i <= 5; i++)
{
    for (int j = 0; j <= 5; j++)
    {
        Console.Write("[*]");
    }
    Console.WriteLine();
}



/* 
do
{
    number++;

    if (number % 2 == 1)
    {
        Console.WriteLine($"{number} {name}");
    }
} while (number <= 7);
 */