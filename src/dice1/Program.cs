﻿using DiceGame;

Console.WriteLine("#### Dice Game ####");

Dice die = new Dice();
die.Roll();
Console.WriteLine(die);

DiceGraphic die1 = new DiceGraphic();
die1.Roll();
Console.WriteLine(die1);

Console.WriteLine("#### Dice Hand ####");
DiceHand hand = new DiceHand();

hand.Add(die);
hand.Add(die1);
hand.Add(new DiceGraphic());
hand.Add(new Dice());
hand.Add(new DiceGraphic());

IDiceInterface die2 = new Dice();
IDiceInterface die3 = new DiceGraphic();

hand.Add(die2);
hand.Add(die3);

hand.Roll();

Console.WriteLine(hand);
Console.WriteLine(hand.Values());

