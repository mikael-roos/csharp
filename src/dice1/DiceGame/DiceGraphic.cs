namespace DiceGame;

class DiceGraphic : Dice
{
    char[] _graphic = {'⚀', '⚁', '⚂', '⚃', '⚄', '⚅'};

    public override string ToString()
    {
        return _graphic[_value - 1].ToString();
    }

    public override string Representation ()
    {
        return _graphic[_value - 1].ToString();
    }
}