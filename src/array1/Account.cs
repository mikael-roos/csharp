class Account
{
    private double _balance = 0.0;
    private string _name;

    private static int _numAccounts = 0;

    public Account(string name)
    {
        this._name = name;
        _numAccounts++;
    }

    public Account(string name, double balance)
    {
        this._name = name;
        this._balance = balance;
        _numAccounts++;
    }

    public void PrintBalance()
    {
        DateTime now = DateTime.Now;
        Console.WriteLine(
            "{0} [{2}] Balance is currently: {1:0.00}",
            now,
            _balance,
            _name
        );
    }

    public override string? ToString()
    {
        DateTime now = DateTime.Now;
        return $"{now} [{_name}] Balance is currently: {_balance:0.00}";
    }

    public void Deposit(double amount)
    {
        _balance += amount;
        PrintBalance();
    }

    public void Withdrawel(double amount)
    {
        _balance -= amount;
        PrintBalance();
    }

    public void SetBalance(double balance)
    {
        _balance = balance;
    }

    public double GetBalance()
    {
        return _balance;
    }

    public void GetBalance(out double balance)
    {
        balance = _balance;
    }

    public double Balance
    {
        get => _balance;
        set
        {
            if (value > 0)
            {
                _balance = value;
            }
            else 
            {
                throw new Exception("Not a valid value for the balance");
            }
        }
    }

    public double NumAccounts
    {
        get => _numAccounts;
    }
}