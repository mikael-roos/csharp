﻿Console.WriteLine("# Array");

int[] a1 = new int[] {1, 2, 3};

Console.WriteLine(a1);

foreach(var item in a1)
{
    Console.WriteLine(item);
}

// a2
int?[] a2 = new int?[5];
Console.WriteLine(a2);

a2[0] = -1;
a2[2] = 2;
a2[3] = 3;
a2[a2.Length - 1] = 99;

a2[0] = 0;
a2[0] = null;

foreach(var item in a2)
{
    Console.WriteLine(item);
}

// Account
Account my = new Account("my", 100);
Account yo = new Account("yo");

Account[] accounts = new Account[2];

accounts[0] = my;
accounts[1] = yo;

Console.WriteLine(accounts);
Console.WriteLine(accounts.Length);
//my.PrintBalance();
//accounts[0].PrintBalance();

foreach(var item in accounts)
{
    //item.PrintBalance();
    Console.WriteLine(item);
}

Array.ForEach(accounts, Console.WriteLine);
