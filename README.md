# Learn programming in C# (Csharp)

[![pipeline status](https://gitlab.com/mikael-roos/csharp/badges/main/pipeline.svg)](https://gitlab.com/mikael-roos/csharp/-/commits/main)

Material to learn C#.

<!--
website: https://mikael-roos.gitlab.io/csharp
-->
