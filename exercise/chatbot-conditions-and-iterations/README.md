---
revision:
    "2023-01-03": "(A, mos) First release."
---
Let ChatBot work with conditions and iterations
==========================

Learn to use conditional statements such as `if` and `switch` together with iterations like `while`, `do while` and `for`.

[[_TOC_]]

This is how the code can look like in this exercise.

![Done](img/done.png)



<!--
Recording
---------------------------

This is a recording when Mikael goes through this exercise, step by step (27 min).

[![2022-11-29](https://img.youtube.com/vi/1-4kfdEimBU/0.jpg)](https://www.youtube.com/watch?v=1-4kfdEimBU)
-->



Precondition
--------------------------

You have worked through the example in the [Structure the Chatbot into functions and files](../../exercise/chatbot-structure-files-and-functions/README.md).

It is the code from that example that we are going to continue to work on.



Prepare
--------------------------

You can read on [iteration statements in C#](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/iteration-statements).



Add a class for this exercise
--------------------------

To keep good organisation of the code we add a separate submenu for this exercise and put it in its own namespace.

Something like this within the namespace `Mos.ConditionIteration` and all the files stored in a subdirectory named `ConditionIteration/`.

```csharp
namespace Mos.ConditionIteration;

class Menu {

    public static void Run(string label = "prompt") {
        string input;
        string prompt = $"[{label}] ";
        
        PrintBanner();
        Console.WriteLine("Write what to do, or enter m/menu/h/help for your options.");

        while (true) {
            Console.Write(prompt);
            input = Console.ReadLine() ?? "";

            switch (input) {
                case "1":
                    ;
                    break;
                case "2":
                    ;
                    break;
                case "3":
                    ;
                    break;
                case "help":
                case "h":
                case "menu":
                case "m":
                    PrintMenu();
                    break;
                case "back":
                case "b":
                case "quit":
                case "q":
                    Console.WriteLine("Bye bye");
                    return;
                default:
                    Console.WriteLine("Not a valid choice, try again or m, menu to show the menu!");
                    break;
            }
        }
    }

    private static void PrintMenu() {    }

    public static void PrintBanner() {    }
}
```

The code above can now be called from the original main program, something like this.

```csharp
    case "5":
        ConditionIteration.Menu.Run(nickname);
        break;
```

This is how you can combine namespaces and subdirectories to structure your code. The namespace directly relates to the code structure in directories.



Use radom and a card deck
--------------------------

To have some fun code to use for this example we are going to simulate a deck of cards like this.

* Random a value between 1 and 52, which is the card.
* 1 is Ace and 13 is the King.
* 1-13 is hearts,
* 14-26 is spades,
* 27-39 is diamonds,
* 40-42 is clubs.

Lets put this in its own class, something like this.

```csharp
namespace Mos.ConditionIteration;

class Card {

    byte card;

    public byte Flip() {
        var rand = new Random();

        card = (byte) rand.Next(1, 52);
        return card;
    }
}
```

Now we have a class we can use to generate a card in a deck of cards. It can look like this when we use it.

```csharp
Card card = new Card();
byte valueOfCard = card.Flip();
Console.WriteLine($"The card is now value {valueOfCard}.");
```

Lets use this deck class to work with conditionals and iterations.



if, else if, else
--------------------------

Lets add a new method to the Card class which returns the name of its suite. For this we can use the if-condition, something like this.

```csharp
public string GetSuiteName() {
    string suite;
    if (card >= 1 && card <= 13) {
        suite = "heart";
    } else if (card >= 14 && card <= 26) {
        suite = "spade";
    } else if (card >= 27 && card <= 39) {
        suite = "diamonds";
    } else {
        suite = "clubs";
    }

    return suite;
}
```

Can you rewrite the if-statement to avoid the else-part?

Can you rewrite the if-statement somehow, to get better looking code? Or perhaps should we just leave it as it is for now.



switch
--------------------------

The menu part is one valid example on where the switch-condition is quite helpful. It is an example where you test towards the same expression/varible and you do different things depending on the value.

Another example is if we want to get the color of the card above.

If we do that using an if-statement if could look like this.

```csharp
public string GetColor() {
    string color;
    if ((card >= 1 && card <= 13) || (card >= 27 && card <= 39)) {
        color = "red";
    } else if ((card >= 14 && card <= 26) || (card >= 40 && card <= 52)) {
        color = "black";
    } else {
        color = "unknown";
    }

    return color;
}
```

We can then rewrite this and implement as a switch-statement like this.

```csharp
public override string? ToString () {
    string face = GetFace();
    string color = GetColor();
    string suite = GetSuiteName();

    return $"({card}) {face} of {suite} ({color})";
}
```

Which variant do you think lokks the best and is most easy to read?



ToString()
--------------------------

Any object can use the method `ToString()` to return a string representation of the object. Lets make a programming exercise where we want to print out the current value of the card and saying if it is a A, Kn, D or K or its value together with its color and suite.

Can you combine your knowledge of if and switch to do a nice implementation of the `ToString()` method?

The output could look something like this.

```text
(3) 3 of heart (red)
(33) 7 of diamonds (red)
(37) J of diamonds (red)
```

The method could look like this.

```csharp

```

<!--
Exercise
--------------------------

* Which card is the highest card?
-->



while
--------------------------

We have seen how an eternal loop is used together with the menu.

```csharp
while (true) {
    // Do the menu
}
```

Now lets us a while loop to draw some cards. Lets say that we shall flip cards until the sum of the cards reach 21 or above. Lets make it easy and consider the Ace to always have the alue of 1.

The code can look something like this.

```csharp
Console.WriteLine($"Draw cards until the sum reaches 21 or above.");
int sum = 0;
while (sum < 21) {
    card.Flip();
    sum += card.GetValue();
    Console.WriteLine(card);
    Console.WriteLine($"Sum is: {sum}.");
}
```

The output could look like this.

```text
(3) 3 of heart (red)
Sum is: 3.
(33) 7 of diamonds (red)
Sum is: 10.
(37) J of diamonds (red)
```

That was an example of the while loop.



do while
--------------------------

The do-loop is like the while loop with an expression that evaluates how long the loop will continue, but the expression is last in the statement so the loop is always executed at least once.

Lets create an example where we flip a card and then we continue to flip a card as long as the card value is higher or equal to the last flipped card.

The code can look something like this.

```csharp
byte lastValue = 0;
byte currentValue = 0;
do {
    lastValue = currentValue;
    card.Flip();
    currentValue = card.GetValue();
    Console.WriteLine(card);
} while (currentValue >= lastValue);
```

The output could look like this.

```text
Draw cards as long as its higher than the card before.
(3) 3 of heart (red)
(30) 4 of diamonds (red)
(17) 4 of spade (black)
(19) 6 of spade (black)
(23) 10 of spade (black)
(51) D of clubs (black)
(48) 9 of clubs (black)
```

You can usually construct your code using any of the while or a do-loop, so choose the one you prefer at each time.



break and continue
--------------------------

The keyword `break` means you can break out from the loop.

The keyword `continue` means you can continue with the next loop round, without executing the final steps of the current loop round.

Lets say that we continue with the code from the do-part and we add the following rules.

* A card with 7 or A is always free (reset game and continue with the next loop round)
* The card Kn of spade will result in end of game (break)

The updated loop can look something like this.

```csharp
Console.WriteLine($"Draw cards as long as its higher than the card before.");
Console.WriteLine($"Card 7 is free and Kn of Space is end of game.");
lastValue = 0;
currentValue = 0;
do {
    lastValue = currentValue;
    card.Flip();
    currentValue = card.GetValue();
    Console.WriteLine(card);
    if (currentValue == 7 || currentValue == 1) {
        currentValue = lastValue = 0;
        continue;
    }
    if (card.GetFace() == "J" && card.GetSuiteName() == "spade") {
        break;
    }
} while (currentValue >= lastValue);
```

The code is starting to grow and the structure of it becomes more important (just a notice).

The output from a run could look like this.

```text
Draw cards as long as its higher than the card before.
Card 7 is free and Kn of Space is end of game.
(42) 3 of clubs (black)
(50) J of clubs (black)
(1) A of heart (red)
(11) J of heart (red)
(20) 7 of spade (black)
(38) D of diamonds (red)
(40) A of clubs (black)
(47) 8 of clubs (black)
(31) 5 of diamonds (red)
```



for
--------------------------

The for-loop is useful when you operate on an variable an incrementing/decremeting it. As an example we can use it to flip 5 cards.

The loop can look like this.

```csharp
Console.WriteLine($"Draw a handfull of cards.");
for (int i = 0; i < 5; i++) {
    card.Flip();
    Console.WriteLine(card);
}
```

The output could look like this.

```text
Draw a handfull of cards.
(44) 5 of clubs (black)
(11) J of heart (red)
(16) 3 of spade (black)
(15) 2 of spade (black)
(23) 10 of spade (black)
```



UTF-8 characters
--------------------------

We can add the suite as a UTF-8 character and make the card a bit more nice looking. The UTF-8 character looks like this.

* ♥
* ♠
* ♦
* ♣

We can add another method that just prints the card a little more card like. It can look like this when dealing a hand of five cards.

```text
Draw a handfull of cards.
[6♠][A♠][8♠][J♠][K♠]
```



A loop in a loop
--------------------------

We can have loops in loops to for example deal several hands for many players. Lets make a for-loop within the for-loop to deal one hand to the player and one hand to the computer, just like they were playing a game of cards.

The code can look like this.

```csharp
Console.WriteLine($"Draw two hands with 5 cards.");
for (int j = 1; j <= 2; j++) {
    Console.WriteLine($"Dealing hand  {j}.");
    for (int i = 0; i < 5; i++) {
        card.Flip();
        Console.Write(card.GetCard());
    }
    Console.WriteLine();
}
```

The output can look like this.

```text
Draw two hands with 5 cards.
Dealing hand  1.
[K♠][4♠][6♠][8♥][9♠]
Dealing hand  2.
[2♥][A♥][K♠][10♥][A♠]
```

Who has the highest hand? Well, that might be a bit harder to solve, so lets leave that for now.



Datastructure versus code
--------------------------

When we started this example we decided on letting the card be representad by a number between 1 to 52. That was a decision on the data structure of our card.

Deciding such a thing is an important programming decision. The structure you decide will result in different coding. Consider if you decided to represent the card as a int for the value and a string for the suite, would that improve your code?

Perhaps, perhaps not.

When you code you usually start with a approach that you think works for your datastructures. Then, as the code grows, you might see another and better approach and you change your datastructure and your code.

That is just the way of the programmer. Sometimes it is a good way to code to update your datastructure and code when you see how it can be improved.



<!--
Calculate on when to becoming a millionare
--------------------------

* Ränta på ränta, när blir jag millionär

You can read about how to calculate "[Compound interest](https://en.wikipedia.org/wiki/Compound_interest)", the basic formula is like this.

```csharp
saveYearly * (1 + interest) ^ years;
```

You can use the built in mathematical functions of `Math.Pow()` and `Math.Floor()` to ease your calculation.



Quote of the day
--------------------------


Guess my number
--------------------------

-->



Summary
--------------------------

We have worked through the basics on conditions and iterations which are important building stones to more advanced programming.
