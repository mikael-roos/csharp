---
revision:
    "2022-12-28": "(A, mos) First version."
---
Lab environment
===========================

This is the lab environment that is recommended for this course.

[[_TOC_]]



Install the latest version
---------------------------

The [latest version](https://dotnet.microsoft.com/en-us/download/dotnet) is .NET 7 and C# 11 that was released 2022.

You need to install the .NET SDK and not just the runtime.

Follow below sections to install .NET SDK for your environment together with a texteditor or Integrated Development Environment (IDE).

Earlier versions will most likely also work, but it is best if you use the latest possible version.



### Windows

Read the whole section before you start to install.

Read about [how to install on your Windows environment](https://learn.microsoft.com/en-us/dotnet/core/install/windows).

The easiest approch might be to install .NET SDK as part ot the development enviroment and there are two to choose from.

* [Install with Visual Studio](https://learn.microsoft.com/en-us/dotnet/core/install/windows?tabs=net70#install-with-visual-studio)
* [Install alongside Visual Studio Code](https://learn.microsoft.com/en-us/dotnet/core/install/windows?tabs=net70#install-alongside-visual-studio-code)

Choose "Visual Studio" if you have no particular preferences.



### MacOS

Read the whole section before you start to install.

Read about [how to install on you Mac OS environment](https://learn.microsoft.com/en-us/dotnet/core/install/macos).

The easiest approch might be to install .NET SDK as part ot the development enviroment and there are two to choose from.

* [Install with Visual Studio for Mac](https://learn.microsoft.com/en-us/dotnet/core/install/macos#install-with-visual-studio-for-mac)
* [Install alongside Visual Studio Code](https://learn.microsoft.com/en-us/dotnet/core/install/macos#install-alongside-visual-studio-code)

Choose "Visual Studio for Mac" if you have no particular preferences.



### Ubuntu / Linux

Read about the [installation process for Linux distributions](https://learn.microsoft.com/en-us/dotnet/core/install/linux) and select the one appropriate for you.

Add a texteditor or IDE of your choice, for example [Visual Studio Code](https://code.visualstudio.com/).



Hello World - Verify that the environment works
---------------------------

The basic look of a Hello World program for C# might look like this.

```csharp
/**
 * The hello World for C# version 11
 */
System.Console.WriteLine("Hello World!");
```

This is how to create it and execute it.

Open your terminal and execute the following command.

```text
dotnet new console -o HelloWorldApp -f net7.0
```

Move into the directory that was created and open your texteditor in that directory.

Find the file `Program.cs` and inspect it.

Now run the program by entering the following the your terminal.

```text
dotnet run
```

It might look like this (in Visual Studio Code).

![HelloWorldApp](img/hello-world-app.png)

Walk through the following tutorial to find out a bit more on how it works.

* [.NET Tutorial - Hello World in 5 minutes](https://dotnet.microsoft.com/en-us/learn/dotnet/hello-world-tutorial/intro)



Where to go from here?
---------------------------

Here are a few things you could do to learn a bit about your development environment.



### Learn more on your texteditor or IDE

Look around your texteditor/IDE and get aquainted to it and what you can do with it.



### Try out some commands in the terminal

You may be working in a Windows terminal or a Mac/Linux terminal. Learn your way with the terminal and try out basic commands to view files and move around in the directory hierachy.



### Start visual code from the terminal

You should be able (most likely) to open a terminal and start vscode from within the terminal using the current working directory `.` (the dot) as the entrypoint.

```
code .
```

That is a good way to use the terminal together with the editor.



### Learn more on C#

There is a playlist "[C# for Beginners](https://learn.microsoft.com/sv-se/shows/CSharp-101/)" where you can get an insight into the basics of the programming language and its constructs.
