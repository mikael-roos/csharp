---
revision:
    "2023-04-21": "(A, mos) For spring 2023."
---
Build desktop applications with C# and .NET
===========================

This exercise will guide you into creating a desktop application for Windows and Mac using .NET Multi-platform App UI (MAUI).

![entry input done](.img/update-entry-field-done.png)

[[_TOC_]]


<!--
TODO

* How to learn more on layout, navigation between pages working with views and data?
* Practice the 7 most common controls, perhaps to build a CRUD application?
* How to use Blazor with MAUI?

-->

<!--
Recording
---------------------------
-->

Preconditions
---------------------------

You need to have the latest version of Visual Studio 2022 and you need to have included the component ".NET Multi-platform App UI development".

The details are described in the article "[.NET MAUI Installation](https://learn.microsoft.com/en-us/dotnet/maui/get-started/installation)". You need to work through this article before proceeding.



Code available
---------------------------

The code used while writing this article is available in [`src/maui`](../../src/maui/).



About
---------------------------

The .NET Multi-platform App UI (.NET MAUI) is a cross-platform framework for creating native mobile and desktop apps with C# and XAML.

It supports developing applications for the platforms Mac, Windows, iOS and Android. Linux is not (yet?) supported as a platform.

Have a quick glance into the web page of "[.NET Multi-platform App UI](https://dotnet.microsoft.com/en-us/apps/maui)" to see how .NET MAUI is presented.

Then read through the article "[What is .NET MAUI?](https://learn.microsoft.com/en-us/dotnet/maui/what-is-maui)" to learn the basics of the MAUI architecture and features.

MAUI is the further development (and replacement) of the cross-platform [Xamarin](https://dotnet.microsoft.com/en-us/apps/xamarin).

MAUI can render in a web browser, like a web application, when using MAUI together with [Blazor](https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor). Details on to do that is written in the article "[Build a .NET MAUI Blazor Hybrid app](https://learn.microsoft.com/en-us/aspnet/core/blazor/hybrid/tutorials/maui)".

There is also the Universal Windows Platform (UWP) to consider, it is a platform to develop Windows applications for any device like mobile, tablet, mobile. desktop. It however only supports Windows. Learn more on "[What's a Universal Windows Platform (UWP) app?](https://learn.microsoft.com/en-us/windows/uwp/get-started/universal-application-platform-guide)".

To wrap this up, we also need to menchen the .NET Windows Presentation Foundation (WPF) which is a UI framework where you can create elements such as buttons, inputs and menus. It supports Windows applications only. The article "[Desktop Guide (WPF .NET)](https://learn.microsoft.com/en-us/dotnet/desktop/wpf/overview/)" explains the basics on WPF.

Finally there is the Windows Forms that is another UI framework to create rich desktop client apps for Windows. The article "[Desktop Guide (Windows Forms .NET)](https://learn.microsoft.com/en-us/dotnet/desktop/winforms/overview/)" provides the basics on the technique.

In short, there are several choices and platforms to use when developing desktop applications on Windows. This article however focuses on MAUI.



Build our first MAUI app - Hello world
---------------------------

The article "[.NET MAUI / Get Started/ Build your first app](https://learn.microsoft.com/en-us/dotnet/maui/get-started/first-app)" shows how to build your first MAUI app. Work through that article to get going.

The article contains details to help you trouble shoot if you get into problems. Here are some additional help.

**Can not find MAUI in Visual Studio** You can add the MAUI component through the menu "Tools -> Get Tools and Features...". Find the MAUI component in the list att install it.

**Trouble with package manager** If you get trouble that you have missing packages, then [update the url to the packet manager](https://stackoverflow.com/a/49512432) with the following details.

* Name: nuget.org
* Source: https://api.nuget.org/v3/index.json

When you are done building the application it will open and look like this. Try clicking the button.

![hello world](.img/hello-world.png)

Use the menu to minimize the application or to take up the full screen. Close the application when you are done.



Change the text in the application
---------------------------

Let's change some details in the application to investigate how it is built up.

The file `MainPage.xaml` contains the XAML (XML to describe the UI) description of the main page of the application.

![main page xaml](.img/mainpage-xaml.png)

Try to edit as many parts of the page as you can and then rebuild and run the application.

This is how it looked for me when changing some details and rearranging the order of the items being displayed.

![hello world edit](.img/hello-world-edit.png)

You can edit the XAML file while having the application running and it will make its updates in real time.



Change the callback
---------------------------

When you click the button a callback is called to deal with what should happen.

The definition of the callback is in `MainPage.xaml`

```xaml
<Button
    Clicked="OnCounterClicked"
/>
```

The actual callback method is a C# method and it is implemented in the file `MainPage.xaml.cs`. You can see from its name that it is using the `.cs` extension to its name `MainPage.xaml`.

Try to update the callback method, both writing other text and calculating the clicks in a different way, just to ensure that you can update it.

It can look something like this when you edit the code for the callback.

![callback](.img/callback.png)

You need to rebuild the application for the changes to take effect.



Add a text field to the UI
---------------------------

We shall add a text field where the user can input a value to the application.

We start in the XAML file for MainPage where all the components of the page are defined.

There is a component, or control, called [Entry](https://learn.microsoft.com/en-us/dotnet/maui/user-interface/controls/entry) that provides a single line entry field.

Try to add the following to your XAML and verify that you can see a text entry fiel in your application.

```xaml
<Entry 
    x:Name="EntryField"
    Placeholder="Enter text"
    TextChanged="OnEntryTextChanged"
    Completed="OnEntryCompleted" />
```

Then add the two callbacks, one for when the entry is updated with new text `OnEntryTextChanged` and one for when the user clicks enter in the entry `OnEntryCompleted`.

The callbacks can be implemented like this. Notice that I added a `NoticeField` that I uses to output the results.

```csharp
private void OnEntryTextChanged(object sender, TextChangedEventArgs e)
{	
    NoticeField.Text = $"The entry field was changed from '{e.OldTextValue}' to '{e.NewTextValue}'";
    SemanticScreenReader.Announce(NoticeField.Text);
}

private void OnEntryCompleted(object sender, EventArgs e)
{
    NoticeField.Text = $"The entry field was completed with the value '{EntryField.Text}'";
    SemanticScreenReader.Announce(NoticeField.Text);
}
```

This is how it can look when adding the code.

![entry callback code](.img/update-entry-code.png)

It can look like this when the user inputs text into the entry. The event for `OnEntryTextChanged` is fired on each key press.

![entry input](.img/update-entry-field.png)

When the user presses the enter key the second event for `OnEntryCompleted` is fired.

![entry input done](.img/update-entry-field-done.png)

The actual events that are fired are explained in the documentation for each control. In this example it were the events of `TextChanged` and `Completed`.



MAUI Controls
-------------------

The Entry control is an object being part of the MAUI Controls that build up the UI. The Controls can be divided into the following sections.

* Pages
* Layouts
* Views

The Entry object is part of the Views.

Read more on [MAUI Controls](https://learn.microsoft.com/en-us/dotnet/maui/user-interface/controls/).



Next step
-------------------

To move on from here you might want to learn more on how to create a more complete MAUI app. The article "[Create a .NET MAUI app
](https://learn.microsoft.com/en-us/dotnet/maui/tutorials/notes-app/)" provides the details you need.



Summary
-------------------

This article helped you to get going with your first MAUI app and to navigate in the code and how to change and add details to the UI.
