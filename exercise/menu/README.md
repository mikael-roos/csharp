---
revision:
    "2022-08-26": "(A, mos) For autumn 2022."
---
Create a menu driven application
===========================

This exercise will guide you into creating a menu driven application in Java.

[[_TOC_]]



About
---------------------------

In short, a menu driven application shows you a menu with choices in the terminal and awaits for your input to select a choce.

It will then execute the choice and present the result.

It will then show the menu again.



Example files in repo
---------------------------

The example files is available in the example repo which you can clone.

```
git clone https://gitlab.com/mikael-roos/java.git
cd java
```

You can also view [the repo online in GitLab](https://gitlab.com/mikael-roos/java).



How to work this exercise
---------------------------

There is a solution to this exercise in the directory `sample/menu`, but the intention is that you try this exercise first, without looking at the solution. Perhaps you can do better coding than the solution code?

Write all your code in one file.

Separate your code into several classes. Use at least a Menu class and a Main class.

Put each menu action into a separate method, or a separate class.



Show the menu
---------------------------

When you start your program you should be presented with the menu, something like this.

```
Menu:
1) Get a quote
2) Get a random number between 1 and 100
3) Get date of today
qQ) Quit

Your choice:
```

You can then enter the choice and it will be executed and showing its output.

You may use the [Java TextBlock](https://docs.oracle.com/en/java/javase/15/text-blocks/index.html) `"""` for multiline strings.

Will you use if statements or a switch-case for this?

Will you use a for, while or do loop for this?



Choice 1) Get a quote
---------------------------

When you enter this choice it could look like this.

```
our choice:1
Get busy working
or
get busy coding.
--Unknown

Press "ENTER" to continue...
```

You can use the [Java Scanner](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html) to read input from the user.

When you press enter you will see the menu again.



Choice 2) Get a random number between 1 and 100
---------------------------

When you enter this choice it could look like this.

```
Your choice:2
Your number is '31'.

Press "ENTER" to continue...
```

The method [`Math.random`](https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#random--) will help you to generate random numbers.



Choice 3) Get date of today
---------------------------

When you enter this choice it could look like this.

```
Your choice:3
Today is 2022-08-15
Press "ENTER" to continue...
```

The method [`LocalDate.now()`](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html#now--) will help you to get the current date.



Choice qQ) Quit
---------------------------

When you enter "q" or "Q" the program should end, it could look like this.

```
Your choice:q
Bye bye!
```



Check the solution
---------------------------

Now, check out the solution in `sample/menu` and see if you think you did better coding or get some hints on how to write your code.
