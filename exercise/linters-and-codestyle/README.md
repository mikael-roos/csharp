---
revision:
    "2023-02-03": "(A, mos) First version."
---
C# Linters and Code style
===========================

This is to learn using linters and tools to format the code. This articles shows how to get going with the easy and included parts but will also show the more advanced parts when includeing external linters. 

[[_TOC_]]

<!--

-->


Microsoft .NET and C# code style and conventions
---------------------------

There is a document "[C# Coding Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)" that explains and proposes the code style to use. This is a good starting point.

The above document can be completed with the guidelines and example in the .Net GitHub repo "[C# Coding Style](https://github.com/dotnet/runtime/blob/main/docs/coding-guidelines/coding-style.md)".

> The primary objectives are consistency and readability within your project, team, organization, or company source code.

We can read the [Google codestyle for C#](https://google.github.io/styleguide/csharp-style.html) as a reference and we will see that they refer to the above two documents.



Visual Studio Code: C# extension
---------------------------

When working with C# in the editor Visual Studio Code you usually also use the [C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)

The extension adds tools and debugging utilities that helps you write the code and visualises errors already when you write the code.



Format the code using built in `format`
---------------------------

The [dotnet tool](https://learn.microsoft.com/en-us/dotnet/core/tools/) contains a tool called [`dotnet format`](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-format) which can provide some basic support in formatting your source code.

This is a good step to start a journey into enforcing codestyle and executin linter tools.

You can run the command like this in your project.

```
dotnet format
```

You can try to format the following "bad structured code".

```csharp
int a=2;
for(int i=1;i<9;i++){
Console.WriteLine(i);}
```

The tool with add lines, spaces and tabs and the resulting code will look like this.

```
int a = 2;
for (int i = 1; i < 9; i++)
{
    Console.WriteLine(i);
}
```



To be explained
---------------------------

This article will be updated with the following information.

* Codestyle using `.editorconfig`
* External tool to enforce codestyle
* External linter tool to do static code analysing

<!--
C# Linting and Formatting Tools in 2021
https://dev.to/srmagura/c-linting-and-formatting-tools-in-2021-bna

Codestyle using editorconfig
---------------------------

Code-style rule options
https://learn.microsoft.com/en-us/dotnet/fundamentals/code-analysis/code-style-rule-options

-->

