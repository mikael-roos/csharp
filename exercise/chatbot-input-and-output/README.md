---
revision:
    "2022-12-29": "(A, mos) First release."
---
A Chatbot to learn C# input and output
==========================

This is to learn the basics on how to write a C# program using output to the terminal and input from the terminal together with variables.

[[_TOC_]]



<!--
Recording
---------------------------

This is a recording when Mikael goes through this exercise, step by step (27 min).

[![2022-11-29](https://img.youtube.com/vi/1-4kfdEimBU/0.jpg)](https://www.youtube.com/watch?v=1-4kfdEimBU)
-->



Precondition
--------------------------

You have installed a lab environment with the latest version of .NET and C#.



Prepare
--------------------------

Create the project as a console application and open it up in your texteditor.

```
dotnet new console -o ChatBotApp -f net7.0 
```



Console.WriteLine()
--------------------------

Use [`Console.WriteLine()`](https://learn.microsoft.com/en-us/dotnet/api/system.console.writeline) to print out a welcoming message.

Add an avatar as ASCII art to your chatbot and give it a name. you can find examples on ASCII art on [ASCII Art Archive](https://www.asciiart.eu/) (or google "ascii art").

I will use "Marvin" for my chatbot example. This is how Marvin can look like in ASCII art.

```text
      ,     ,
     (\____/)
      (_oo_)
        (O)
      __||__    \/
   []/______\[] /
   / \______/ \/
  /    /__\
 /\   /____\
```

I can add the multiline Marvin ascii character into a variable using the "[verbatim text @](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim)" like this.

```csharp
string marvin = @"
      ,     ,
     (\____/)
      (_oo_)
        (O)
      __||__    \/
   []/______\[] /
   / \______/ \/
  /    /__\
 /\   /____\
";
```

It can look like this when you are done and when you run your program.

![Marvin Hello](img/marvin-hello.png)



Console.ReadLine()
--------------------------

Use [`Console.ReadLine()`](https://learn.microsoft.com/en-us/dotnet/api/system.console.readline) to read the name of the user and store it in a variable.

We can use [string interpolation](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated) to combine the output of text and variables.

It can look like this when you are done and when you run your program.

![Marvin name](img/marvin-name.png)



string
--------------------------

Read a bit on [strings and the string literal](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/strings/) to learn various ways of using them.



integer
--------------------------

Read a bit on [Integral numeric types](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/integral-numeric-types) to learn about handling of number such as integers.



Calculate with integers
--------------------------

Train the bot to:

* ask for your age and in what age you will retire from work
* calculate how long it is before you can retire
* ask how much money you save each month
* calculate how much you will have on your savings account when you retire
* and calculate at what age you will become a millionar.

You can input a integer from the keyboard like this.

```csharp
int currentAge = Convert.ToInt32(Console.ReadLine());
```

Learn more on [how to format strings using `String.Format()`](https://learn.microsoft.com/en-us/dotnet/api/system.string.format).

Quickly review how "[operator precedence](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/#operator-precedence)" and "[operator associativity](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/#operator-associativity)" affects how an expression is calculated. You may compare it to regular mathematic rules.

It can look like this when you are done and when you run your program.

![Marvin int](img/marvin-int.png)



float
--------------------------

Read a bit on [Floating-point numeric types](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types) to learn about handling of numbers with decimals.



Calculate with float/double
--------------------------

Train the bot to do some calculations on a circle:

* ask for the diameter of the circle (as a double)
* calculate the radius of the circle
* calculate the area of the circle
* calculate the circumference of the circle
* show the value of PI.

You can input a double from the keyboard like this.

```csharp
double diameter = Convert.ToDouble(Console.ReadLine());
```

You can use the built in mathematical functions of `Math.Pow()` and `Math.PI` to ease your calculation.

It can look like this when you are done and when you run your program.

![Marvin circle](img/marvin-circle.png)



Explicit and implicit type conversions
--------------------------

You should be aware of the concept of "[Built-in numeric conversions](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/numeric-conversions)" and how to do explicit numeric conversions or "[Casting and type conversions](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/types/casting-and-type-conversions)".



Built-in objects
--------------------------

You can use the built-in object [DateTime](https://learn.microsoft.com/en-us/dotnet/api/system.datetime) when you want to calculate date and time.

Train the bot to do some calculations on a circle:

* ask for date as "2023-01-16"
* print the current date and time
* convert the date you did input to a DateTime object
* print out details on the date, month and week.

The date you enter should be enter as a string and then parsed to a DateTime object using `DateTime.Parse()`.

You can read on the "[DateTime object](https://learn.microsoft.com/en-us/dotnet/api/system.datetime)" to learn how to use it and its properties.

It can look like this when you are done and when you run your program.

![Marvin date](img/marvin-date.png)



Summary
--------------------------

You have now built and run a C# program with some basic constructs using system input output, built in objects and variables with different types.
