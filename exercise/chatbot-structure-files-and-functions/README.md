---
revision:
    "2022-12-30": "(A, mos) First release."
---
Structure the Chatbot into functions and files
==========================

Learn to structure your code into functions and files.

[[_TOC_]]



<!--
Recording
---------------------------

This is a recording when Mikael goes through this exercise, step by step (27 min).

[![2022-11-29](https://img.youtube.com/vi/1-4kfdEimBU/0.jpg)](https://www.youtube.com/watch?v=1-4kfdEimBU)
-->



Precondition
--------------------------

You have worked through the example in the [A Chatbot to learn C# input and output](../chatbot-input-and-output/README.md).

It is the code from that example that we are going to re-structure into files and functions.



<!--
Prepare
--------------------------

Create the project as a console application and open it up in your texteditor.

```
dotnet new console -o ChatBotApp -f net7.0 
```
-->



Split the Main from the Application
--------------------------

The first thing we can consider is what the actual application code is and what is the main program. The basic idea is to have small main programs and move the application code into its own file(s).

We want to end up with a proper structure where each code section does one thing and has a reasonable size and that the code is readable.

Lets start by checking out what kind of tools we have to restructure our code.

By the way, there is a name for reorganising the code as it grows, its "[code refactoring](https://en.wikipedia.org/wiki/Code_refactoring)" and it is a way to reach good and clean code.



A class with static methods
--------------------------

You can structure code into classes and methods. A class can have several methods. Here is a small example.

```csharp
Console.WriteLine("Hello, World!");

MyClass.Hi();

class MyClass {
    public static void Hi() {
        Console.WriteLine("Hi!");
    }
}
```

You can see a working example in the example program [`Class`](../../src/Class/).



Using a class method when in separate files
--------------------------

You can also separate the code into different files, like this.

This is the main file `Program.cs`.

```csharp
Console.WriteLine("Hello, World!");

MyClass.Hi();
```

This is the class file `MyClass.cs`.

```csharp
class MyClass {
    public static void Hi() {
        Console.WriteLine("Hi!");
    }
}
```

It is a good idea to separate classes into their own files. You will then get sections of code that is more manageble in size and you can focus the code to do "one thing good".

You can see a working example in the example program [`ClassInFile`](../../src/ClassInFile/).



Adding a namespace
--------------------------

Adding a namespace is protecting parts of your code so it does not interfere with other code, this is good practice as the code grows. It can look like this.

This is the class file `MyClass.cs`, now defined under a namespace.

```csharp
namespace Mos;

class MyClass {
    public static void Hi() {
        Console.WriteLine("Hi!");
    }
}
```

This is the main file `Program.cs`, now using that namespace.

```csharp
using Mos;

Console.WriteLine("Hello, World!");

MyClass.Hi();
```

You can see a working example in the example program [`ClassInFileNamespace`](../../src/ClassInFileNamespace/).



Split code into manageble sections
--------------------------

Split the code and move all application code from main into methods in the class. Let the main call the methods that make up the application.

From the main perspective, it could look like this.

```csharp
using Mos;

Menu.PrintBanner();
Menu.GetName();
Menu.PlanRetirement();
Menu.CalculateCircle();
Menu.ShowDates();
```

Or, it could look like this.

```csharp
using Mos;

Menu.Run();
```

Same, same but different, which one do you prefer and why?



Be DRY and add methods when needed
--------------------------

Each part of the original program had a code section printing a header. This can be put into a private method and reused when needed. The result might look like this.

```csharp
private static void PrintHeader(string header) {
    //string header = "Plan the retirement";
    string separator = "**********";

    Console.Write("Press Enter to continue. ");
    Console.ReadLine();
    Console.WriteLine(String.Format("\n{0} {1, -30} {0}", separator, header));
}
```

The method is called like this.

```csharp
public static void PlanRetirement() {
    // Get current age and age of retirement
    string header = "Plan the retirement";
    PrintHeader(header);
}
```



Add a menu and select what to do
--------------------------

Our main is currently running all parts of the application, but what if we added a menu from which we could select what part to do?

This is the current setup.

```csharp
public static void Run() {
    PrintBanner();
    GetName();
    PlanRetirement();
    CalculateCircle();
    ShowDates();
}
```

Now, let the user select what part to do, and run it in an eternal loop.

```csharp
public static void Run() {
    // PrintBanner();
    // GetName();
    // PlanRetirement();
    // CalculateCircle();
    // ShowDates();

    string input;
    string prompt = "[prompt] ";
        
    Console.WriteLine("Write what to do, or enter m/menu/h/help for your options.");

    while (true) {
        Console.Write(prompt);
        input = Console.ReadLine() ?? "";

        switch (input) {
            case "1":
                GetName();
                break;
            case "2":
                PlanRetirement();
                break;
            case "3":
                CalculateCircle();
                break;
            case "4":
                ShowDates();
                break;
            case "help":
            case "h":
            case "menu":
            case "m":
                PrintMenu();
                break;
            case "quit":
            case "q":
                Console.WriteLine("Bye bye");
                return;
            default:
                Console.WriteLine("Not a valid choice, try again or m, menu to show the menu!");
                break;
        }
    }
}
```

Can you do your own menu driven program now?

Should the method `PrintMenu()` be public or private?



How to change the prompt
--------------------------

It would be nice if we could change the prompt to display our nickname. How can we do that?

There might be a couple of obvious solutions, but I will go with the one that returns a value from a function.

In the function `GetName()` we can return the value of the nickname and use the returned value as the prompt.

This is how we can return a value from the function.

```csharp
    public static string GetName() {
        Console.Write("What may I call you? ");

        string nickname = Console.ReadLine() ?? "no nick";

        return nickname ;
    }
```

This is how it could be used on the receiving side.

```csharp
switch (input) {
    case "1":
        string nickname = GetName();
        prompt = $"[{nickname}] ";
        break;
}
```

It might look like this when the code is executed.

![Change nick to prompt](img/change-prompt.png)

Another solution would be to add the variable prompt as a member of the class and change it whenever needed. But lets save that solution for another day.



Summary
--------------------------

We have worked through the basics on how to divide code into manageable sections using files, classes and static methods.

