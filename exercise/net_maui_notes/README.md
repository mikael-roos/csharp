---
revision:
    "2023-04-21": "(A, mos) For spring 2023."
---
Create a Notes application with MAUI
===========================

This exercise will guide you into creating a desktop application for Windows and Mac using .NET Multi-platform App UI (MAUI).

This exercise is based on the larger article "[Create a .NET MAUI app](https://learn.microsoft.com/en-us/dotnet/maui/tutorials/notes-app/)" which you can use as a reference.

<!--
![entry input done](.img/update-entry-field-done.png)
-->

<!--
TODO

Osäker om denna artikel verkligen behövs, det är nog enklare att använda den befintliga artikeln och jobba igenom den så den har så mycket kod och förklaringar, det är svårt att göra den biten bättre.

De steg som krävs för att komma igång innan man gör Notes applikationen är följande.

* [Installation](https://learn.microsoft.com/en-us/dotnet/maui/get-started/installation)
* [Build your first app](https://learn.microsoft.com/en-us/dotnet/maui/get-started/first-app)

Sedan kan man jobba igenom artikeln.

* [Create a .NET MAUI app](https://learn.microsoft.com/en-us/dotnet/maui/tutorials/notes-app/)

Därefter kan man fortsätta med MVVM artikeln.

* [Upgrade your app with MVVM concepts](https://learn.microsoft.com/en-us/dotnet/maui/tutorials/notes-mvvm/)

Man bör också göra ett par övningar som visar hur olika typer av pages, layouts och views (controls) kan fungera tillsammans. Man bör nog även visa igen hur routing mellan sidor kan fungera.

-->

[[_TOC_]]


<!--
TODO

* How to learn more on layout, navigation between pages working with views and data?
* Practice the 7 most common controls, perhaps to build a CRUD application?
* How to use Blazor with MAUI?

-->

<!--
Recording
---------------------------
-->

Preconditions
---------------------------

You need to have the latest version of Visual Studio 2022 and you need to have included the component ".NET Multi-platform App UI development".

The details are described in the article "[.NET MAUI Installation](https://learn.microsoft.com/en-us/dotnet/maui/get-started/installation)". You need to work through this article before proceeding.



Code available
---------------------------

The code used while writing this article is available in [`src/notes`](../../src/notes/).



Create a MAUI application
---------------------------

Create the application.

Run it in the emulator.



Add an AboutPage
---------------------------

```xml
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://schemas.microsoft.com/dotnet/2021/maui"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             x:Class="Notes.AboutPage">
    <VerticalStackLayout Spacing="10" Margin="10">
        <HorizontalStackLayout Spacing="10">
            <Image Source="dotnet_bot.png"
                   SemanticProperties.Description="The dot net bot waving hello!"
                   HeightRequest="64" />
            <Label FontSize="22" FontAttributes="Bold" Text="Notes" VerticalOptions="End" />
            <Label FontSize="22" Text="v1.0" VerticalOptions="End" />
        </HorizontalStackLayout>

        <Label Text="This app is written in XAML and C# with .NET MAUI." />
        <Button Text="Learn more..." Clicked="LearnMore_Clicked" />
    </VerticalStackLayout>
</ContentPage>
```


Create a NotePage
---------------------------

```xml
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://schemas.microsoft.com/dotnet/2021/maui"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             x:Class="Notes.NotePage"
             Title="Note">
    <VerticalStackLayout Spacing="10" Margin="5">
        <Editor x:Name="TextEditor"
                Placeholder="Enter your note"
                HeightRequest="100" />

        <Grid ColumnDefinitions="*,*" ColumnSpacing="4">
            <Button Text="Save"
                    Clicked="SaveButton_Clicked" />

            <Button Grid.Column="1"
                    Text="Delete"
                    Clicked="DeleteButton_Clicked" />
        </Grid>
    </VerticalStackLayout>
</ContentPage>
```



Prepare Pages to be Views
---------------------------

Create the Views directory.

Drag Pages into Views directory.

Update namespace in AboutPage, NotePage and AppShell.

Run in emulator.



Separate View and Model
---------------------------

Define the model.

```csharp
namespace Notes.Models;

internal class Note
{
    public string Filename { get; set; }
    public string Text { get; set; }
    public DateTime Date { get; set; }
}
```

```csharp
namespace Notes.Models;

internal class About
{
    public string Title => AppInfo.Name;
    public string Version => AppInfo.VersionString;
    public string MoreInfoUrl => "https://aka.ms/maui";
    public string Message => "This app is written in XAML and C# with .NET MAUI.";
}
```

Update the AboutPage to bind to the model class.

Run it.

Update the NotePage to bind to the model class.

Run it.



Multiple notes
---------------------------

Add page AllNotesPage in the Views directory.

Add model AllNotes.cs in the Models directory.

Query string parameters.

Register the NotesPage with the navigation system.




Run it in another emulator
---------------------------

Run it in the Android emulator to check it out.



Next step
-------------------

[Upgrade your app with MVVM concepts](https://learn.microsoft.com/en-us/dotnet/maui/tutorials/notes-mvvm/)

Download the code solution to start with.

* https://github.com/dotnet/maui-samples/raw/main/7.0/Tutorials/ConvertToMvvm/step1_upgrade.zip



Summary
-------------------

This article helped you to get going with your first MAUI app and to navigate in the code and how to change and add details to the UI.
